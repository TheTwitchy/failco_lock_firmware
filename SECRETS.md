# FailCo Smart Lock Secrets
DO NOT READ THIS UNLESS YOU WANT TO SPOIL THE METHOD FOR SOLVING EACH LEVEL. YOU HAVE BEEN WARNED.

* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 






















































## v1.0
  * The lock will require the user to go through initial setup to specify a PIN and set the locks name.
  * However, the PIN will only be checked client-side, and the lock_state characteristic will be writable, as long as a specific value is given (which can be gained either through reverse engineering the app, or sniffing).
  * The lock_name characteristic will also be vulnerable to a buffer overflow, but is for later levels.
  * Lock will automatically relock after 5 seconds.

## v2.0
  * The lock_state characteristic will no longer be writable, the PIN characteristic is no longer readable, and there will be a new writable characteristic will be available that accepts PIN attempts.
  * PIN is setup during the first start, and at the conclusion of setup, a flag is set in memory to signify that the lock is initialized, and the PIN is also stored in memory.
  * The attacker needs to setup a MITM connection between the victim and the lock to read the submitted PIN out of the air on an attempt.

## v3.0
  * The lock will now implement encryption during connections. 
  * However, someone accidentally enabled READ access on the PIN characteristic, so you can connect over an unencrypted connection, read the PIN, then send the PIN. 
  
## v4.0
  * The connection between the lock and the app is now encrypted with JustWorks pairing.
  * An attacker can either use something like Crackle to break the weak pairing, or can use the buffer overflow in the lock_name characteristic to overwrite the PIN in memory and then authenticate with the new PIN.