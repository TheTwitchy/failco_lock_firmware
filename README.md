# FailCo Smart Lock CTF 

This is the main repository for the FailCo Smart Lock CTF. This CTF is intended to be a learning tool for both embedded developers who want to learn the types of BLE security controls available for IoT devies, and for hackers who want to learn how to break them.

This CTF is "progressive", in that each level is harder than the last, and builds off the experiences of previous "versions" of the lock. It's intended to be similar to the [Microcorruption CTF](https://microcorruption.com/login).

This repository stores the firmware builds for all levels of the FailCo Lock Bluetooth LE CTF, with v1.0 being the easiest and first level. For accessing the Android app build, please see the repo [here]().

## Firmware Installation

For those who just want to run the build, not develop for or compile it.

### Hardware
You will need:
  * An ESP32 with a UART to USB bridge
    * The [Sparkfun ESP32 Thing](https://www.sparkfun.com/products/13907) already has everything assembled and ready to go, you should only need to connect and flash.
    * Other ESP32 development boards (like [this](https://www.adafruit.com/product/3269)) will likely have similar setups to aid in writing new firmware to the board.
  * An LED connected to GPIO 5 with a small inline resistor (100 to 1000 ohms is probably fine).
    * This acts as a visual indicator of whether the "lock" is engaged or not. The SparkFun ESP32 Thing already has this.
  * Any computer that can host the [ESP-IDF](https://github.com/espressif/esp-idf) and flash the ESP32 board. According to the [docs](https://esp-idf.readthedocs.io/en/latest/get-started/index.html#get-esp-idf), Windows, Linux, or Mac should work fine.
  * A BLE-capable Android device (sorry, no iOS at this time).

### Instructions
  * Follow the instructions from the official ReadTheDocs for version 3.0, including switching over to the v3.0 branch of the IDF from the nightly/master branch.

### Troubleshooting
  * Can open the port /dev/ttyUSB0.
    * This is caused by not having correct permissions to access the device over TTY. 
    * Issue the command ``sudo adduser $USER dialout`` to add yourself to the dialout group.

## Building
Coming soon!
```
python /home/nelendt/apps/esp-idf/components/esptool_py/esptool/esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 115200 --before default_reset --after hard_reset write_flash -z --flash_mode dio --flash_freq 40m --flash_size detect 0x1000 /home/nelendt/apps/failco_lock_firmware/v1.0/build/bootloader/bootloader.bin 0x10000 /home/nelendt/apps/failco_lock_firmware/v1.0/build/gatt_server_demos.bin 0x8000 /home/nelendt/apps/failco_lock_firmware/v1.0/build/partitions_singleapp.bin
```