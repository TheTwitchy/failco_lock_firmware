#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_bt.h"
#include "bta_api.h"

#include "esp_gap_ble_api.h"
#include "esp_gatts_api.h"
#include "esp_bt_defs.h"
#include "esp_bt_main.h"

#include "nvs_flash.h"
#include "nvs.h"

#include "sdkconfig.h"

//#include "bler.h"

#define LOG_TAG "FC_LOCK_v1.0"

///Declare the static function 
static void lock_service_event_handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param);


#define LOCK_NAME_GATT                      "FailCo Lock v1"

#define LOCK_NAME_PERSONALIZED_DEFAULT      "Unnamed Smart Lock"
#define LOCK_NAME_PERSONALIZED_MAX_LEN      40
#define LOCK_STATE_MAGIC_VALUE_LOCKED       0x00c017fa
#define LOCK_STATE_MAGIC_VALUE_UNLOCKED     0xffc017fa

#define LOCK_SERVICE_UUID                   0xfa17

#define LOCK_SERVICE_CHARS_NUM              3

#define LOCK_INIT_CHAR_UUID                 0xff01
#define LOCK_INIT_CHAR_INDEX                0
#define LOCK_INIT_NVS_HANDLE_NAME           "lock_init"
// This magic value should change between versions. The NVS is not cleared between different versions of the lock, so going from one level to another you may have data stored from the previous lock. This value will ensure that if you change levels, the previous NVS values will be cleared properly when starting a new level. This isn't *technically* a part of the CTF, but just more so you don't have to manually clear NVS between versions.
#define LOCK_INIT_MAGIC_VALUE               0x11111111

#define LOCK_STATE_CHAR_UUID                0xff02
#define LOCK_STATE_CHAR_INDEX               1
#define LOCK_GPIO                           5
#define GPIO_HIGH                           1
#define GPIO_LOW                            0

//#define LOCK_NAME_CHAR_UUID                 0xff03
//#define LOCK_NAME_CHAR_INDEX                2
//#define LOCK_NAME_NVS_HANDLE_NAME           "lock_name"

#define USER_PIN_CHAR_UUID                  0xff03
#define USER_PIN_CHAR_INDEX                 2
#define USER_PIN_NVS_HANDLE_NAME            "user_pin"


#define LOCK_AUTOLOCK_TIME                  5

typedef struct {
    esp_bt_uuid_t char_uuid;
    uint16_t attr_handle;
    esp_attr_value_t char_val;
    esp_gatt_perm_t permissions;
    esp_gatt_char_prop_t properties;
    uint16_t is_stored;
    nvs_handle storage_handle;
    char storage_handle_name[16];

} gatts_char_t;

 struct gatts_profile_inst{
    esp_gatts_cb_t gatts_cb;
    uint16_t gatts_if;
    uint16_t app_id;
    uint16_t conn_id;
    uint16_t service_handle;
    esp_gatt_srvc_id_t service_id;
} ;

// Lock service characteristic definitions
// Y U NO SEMAPHORE? - This is basically a single core, so there's no point at which it will be both read and written to. There might be the chance for some weirdness if one task is scheduled out in the middle of a read operation, but it's *probably* nothing.
uint32_t global_lock_init = 0;
uint32_t global_lock_state = (uint32_t) LOCK_STATE_MAGIC_VALUE_LOCKED;
uint32_t global_user_pin = (uint32_t) 0x34333231;
uint32_t global_autolock_counter = 0;

static gatts_char_t lock_service_chars[LOCK_SERVICE_CHARS_NUM] = {
    // Lock initialized characteristic
    [LOCK_INIT_CHAR_INDEX] = {
        .char_uuid = {
            .len = ESP_UUID_LEN_16,
            .uuid = {
                .uuid16 = LOCK_INIT_CHAR_UUID,
            }
        },
        .attr_handle = 0, //Filled out later
        .permissions = ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE, 
        .properties = ESP_GATT_CHAR_PROP_BIT_READ | ESP_GATT_CHAR_PROP_BIT_WRITE | ESP_GATT_CHAR_PROP_BIT_NOTIFY,
        .char_val = {
            .attr_max_len = sizeof(uint32_t),
            .attr_len = sizeof(global_lock_init),
            .attr_value = (uint8_t *)&global_lock_init,
        },
        .is_stored = 1,
        .storage_handle_name = LOCK_INIT_NVS_HANDLE_NAME,
    },

    // Lock state characteristic
    [LOCK_STATE_CHAR_INDEX] = {
        .char_uuid = {
            .len = ESP_UUID_LEN_16,
            .uuid = {
                .uuid16 = LOCK_STATE_CHAR_UUID,
            }
        },
        .attr_handle = 0, //Filled out later
        .permissions = ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE, 
        .properties = ESP_GATT_CHAR_PROP_BIT_READ | ESP_GATT_CHAR_PROP_BIT_WRITE | ESP_GATT_CHAR_PROP_BIT_NOTIFY,
        .char_val = {
            .attr_max_len = sizeof(uint32_t),
            .attr_len = sizeof(global_lock_state),
            .attr_value = (uint8_t *)&global_lock_state,
        },
        .is_stored = 0,
    },

    /*
    // Lock name characteristic
    [LOCK_NAME_CHAR_INDEX] = {
        .char_uuid = {
            .len = ESP_UUID_LEN_16,
            .uuid = {
                .uuid16 = LOCK_STATE_CHAR_UUID,
            }
        },
        .attr_handle = 0, //Filled out later
        .permissions = ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE, 
        .properties = ESP_GATT_CHAR_PROP_BIT_READ | ESP_GATT_CHAR_PROP_BIT_WRITE | ESP_GATT_CHAR_PROP_BIT_NOTIFY,
        .char_val = {
            .attr_max_len = 40,
            .attr_len = sizeof(uint32_t),
            .attr_value = (uint8_t *)&global_user_pin, //TODO FIX
        },
        .is_stored = 0,
        .storage_handle_name = LOCK_NAME_NVS_HANDLE_NAME,
    },
    */

    // User PIN characteristic
    [USER_PIN_CHAR_INDEX] = {
        .char_uuid = {
            .len = ESP_UUID_LEN_16,
            .uuid = {
                .uuid16 = USER_PIN_CHAR_UUID,
            }
        },
        .attr_handle = 0, //Filled out later
        .permissions = ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE, 
        .properties = ESP_GATT_CHAR_PROP_BIT_READ | ESP_GATT_CHAR_PROP_BIT_WRITE | ESP_GATT_CHAR_PROP_BIT_NOTIFY,
        .char_val = {
            .attr_max_len = sizeof(uint32_t),
            .attr_len = sizeof(global_user_pin),
            .attr_value = (uint8_t *)&global_user_pin,
        },
        .is_stored = 1,
        .storage_handle_name = USER_PIN_NVS_HANDLE_NAME,
    },
};

// Nicely structured data for the advertising packet, instead of bitbanging
#define GAP_ADV_SERVICE_LEN       16
static uint8_t gap_adv_service_uuid[GAP_ADV_SERVICE_LEN] = {
    /* LSB <--------------------------------------------------------------------------------> MSB */
    0xfb, 0x34, 0x9b, 0x5f, 0x80, 0x00, 0x00, 0x80, 0x00, 0x10, 0x00, 0x00, 0xbf, 0xbf, 0x00, 0x00,
};

// GAP Advertising data
static esp_ble_adv_data_t lock_adv_data = {
    .set_scan_rsp = false,
    .include_name = true,
    .include_txpower = true,
    .min_interval = 0x20,
    .max_interval = 0x40,
    .appearance = 0x00,
    .manufacturer_len = 0, 
    .p_manufacturer_data =  NULL,
    .service_data_len = 0,
    .p_service_data = NULL,
    .service_uuid_len = GAP_ADV_SERVICE_LEN,
    .p_service_uuid = gap_adv_service_uuid,
    .flag = (ESP_BLE_ADV_FLAG_GEN_DISC | ESP_BLE_ADV_FLAG_BREDR_NOT_SPT),
};

// Standard advertising parameters (type 0x01)
static esp_ble_adv_params_t lock_adv_params = {
    .adv_int_min        = 0x20,
    .adv_int_max        = 0x40,
    .adv_type           = ADV_TYPE_IND,
    .own_addr_type      = BLE_ADDR_TYPE_PUBLIC,
    //.peer_addr            =
    //.peer_addr_type       =
    .channel_map        = ADV_CHNL_ALL,
    .adv_filter_policy = ADV_FILTER_ALLOW_SCAN_ANY_CON_ANY,
};



/* One gatt-based profile one app_id and one gatts_if, this array will store the gatts_if returned by ESP_GATTS_REG_EVT */
static struct gatts_profile_inst lock_service_profile = {
    .gatts_cb = lock_service_event_handler,
    .gatts_if = ESP_GATT_IF_NONE,       /* Not get the gatt_if, so initial is ESP_GATT_IF_NONE */
};

static int uuid_equals(esp_bt_uuid_t * uuid1, esp_bt_uuid_t * uuid2){
    if (uuid1->len != uuid2->len){
        return 0;
    }

    if (uuid1->len == ESP_UUID_LEN_16){
        if (uuid1->uuid.uuid16 == uuid2->uuid.uuid16){
            return 1;
        }
        return 0;
    }

    if (uuid1->len == ESP_UUID_LEN_32){
        if (uuid1->uuid.uuid32 == uuid2->uuid.uuid32){
            return 1;
        }
        return 0;
    }
    //TODO Implement 128 UUID checking

    return 0;
}

static void gap_event_handler(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t *param)
{
    switch (event) {
    case ESP_GAP_BLE_ADV_DATA_SET_COMPLETE_EVT:
        esp_ble_gap_start_advertising(&lock_adv_params);
        break;
    case ESP_GAP_BLE_ADV_DATA_RAW_SET_COMPLETE_EVT:
        esp_ble_gap_start_advertising(&lock_adv_params);
        break;
    case ESP_GAP_BLE_SCAN_RSP_DATA_RAW_SET_COMPLETE_EVT:
        esp_ble_gap_start_advertising(&lock_adv_params);
        break;
    case ESP_GAP_BLE_ADV_START_COMPLETE_EVT:
        //advertising start complete event to indicate advertising start successfully or failed
        if (param->adv_start_cmpl.status != ESP_BT_STATUS_SUCCESS) {
            ESP_LOGE(LOG_TAG, "Advertising start failed");
        }
        break;
    case ESP_GAP_BLE_ADV_STOP_COMPLETE_EVT:
        if (param->adv_stop_cmpl.status != ESP_BT_STATUS_SUCCESS) {
            ESP_LOGE(LOG_TAG, "Advertising stop failed");
        }
        else {
            ESP_LOGI(LOG_TAG, "Stop adv successfully");
        }
        break;
    default:
        break;
    }
}

static void lock_service_event_handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param) {
    int n;

    switch (event) {
    case ESP_GATTS_REG_EVT:
        ESP_LOGI(LOG_TAG, "REGISTER_APP_EVT, status %d, app_id %d", param->reg.status, param->reg.app_id);
        lock_service_profile.service_id.is_primary = true;
        lock_service_profile.service_id.id.inst_id = 0x00;

        esp_ble_gap_set_device_name(LOCK_NAME_GATT);
        esp_ble_gap_config_adv_data(&lock_adv_data);
        esp_ble_gatts_create_service(gatts_if, &lock_service_profile.service_id, 1 + (LOCK_SERVICE_CHARS_NUM * 3));
        break;
    case ESP_GATTS_READ_EVT: {
        ESP_LOGI(LOG_TAG, "GATT_READ_EVT, conn_id %d, trans_id %d, handle 0x%02x", param->read.conn_id, param->read.trans_id, param->read.handle);
        esp_gatt_rsp_t rsp;
        memset(&rsp, 0, sizeof(esp_gatt_rsp_t));
        rsp.attr_value.handle = param->read.handle;

        // Search all characteristics for a matching handle. 
        for (n = 0; n < LOCK_SERVICE_CHARS_NUM; n++){
            if (param->read.handle == lock_service_chars[n].attr_handle){
                rsp.attr_value.len = lock_service_chars[n].char_val.attr_len;
                memcpy(&rsp.attr_value.value, lock_service_chars[n].char_val.attr_value, lock_service_chars[n].char_val.attr_len);
                break;
            }
        }

        esp_ble_gatts_send_response(gatts_if, param->read.conn_id, param->read.trans_id,
                                    ESP_GATT_OK, &rsp);
        break;
    }
    case ESP_GATTS_WRITE_EVT: {
        ESP_LOGI(LOG_TAG, "GATT_WRITE_EVT, conn_id %d, trans_id %d, handle 0x%02x", param->write.conn_id, param->write.trans_id, param->write.handle);
        ESP_LOGI(LOG_TAG, "GATT_WRITE_EVT, value len %d, value %08x", param->write.len, *(uint32_t *)param->write.value);
        
        // Search all characteristics for a matching handle. 
        for (n = 0; n < LOCK_SERVICE_CHARS_NUM; n++){
            if (param->write.handle == lock_service_chars[n].attr_handle){
                memcpy(lock_service_chars[n].char_val.attr_value, param->write.value, param->write.len);
                lock_service_chars[n].char_val.attr_len = param->write.len;

                // Check to see if the value is stored, and push into NVRAM if so

                if (lock_service_chars[n].is_stored){
                    // TODO I barfed a little bit writing this, this is awful. There should be another way to determine the blob length, other than having the max len written to it. This has the possibility to overread the value being written. This current limitation means that you can only write a value to a characteristic if it is exactly the length of the max length.
                    if (lock_service_chars[n].char_val.attr_max_len == param->write.len){
                        ESP_LOGI(LOG_TAG, "WRITING INT TO STORAGE")
                        nvs_set_blob(lock_service_chars[n].storage_handle, lock_service_chars[n].storage_handle_name, param->write.value, lock_service_chars[n].char_val.attr_max_len);
                        //nvs_set_blob(lock_service_chars[n].storage_handle, lock_service_chars[n].storage_handle_name, 0xabcdef01, );
                        nvs_commit(lock_service_chars[n].storage_handle);
                        //nvs_close(lock_service_chars[n].storage_handle);
                    }
                }

                break;
            }
        }
        esp_ble_gatts_send_response(gatts_if, param->write.conn_id, param->write.trans_id, ESP_GATT_OK, NULL);
        break;
    }
    case ESP_GATTS_CREATE_EVT:
        ESP_LOGI(LOG_TAG, "CREATE_SERVICE_EVT, status %d,  service_handle %d", param->create.status, param->create.service_handle);
        lock_service_profile.service_handle = param->create.service_handle;

        esp_ble_gatts_start_service(lock_service_profile.service_handle);

        // Add each characteristic to the lock service
        int n = 0;
        for (; n < LOCK_SERVICE_CHARS_NUM; n++){
            // Add each characteristic to the service
            esp_ble_gatts_add_char(
                param->create.service_handle, 
                &lock_service_chars[n].char_uuid, 
                lock_service_chars[n].permissions, 
                lock_service_chars[n].properties, 
                &lock_service_chars[n].char_val, 
                NULL);
        }
        break;
    
    case ESP_GATTS_ADD_CHAR_EVT: {

        ESP_LOGI(LOG_TAG, "ADD_CHAR_EVT, status %d,  attr_handle %d, service_handle %d",
                param->add_char.status, param->add_char.attr_handle, param->add_char.service_handle);

        //Here we are populating attribute handles after the characteristic has been created (as these are assigned dynamically at runtime).
        n = 0;
        for (;n < LOCK_SERVICE_CHARS_NUM; n++){
            if (uuid_equals(&(param->add_char.char_uuid), &(lock_service_chars[n].char_uuid))){
                lock_service_chars[n].attr_handle = param->add_char.attr_handle;
                break;
            }
        }
        break;
    }
    case ESP_GATTS_ADD_CHAR_DESCR_EVT:
        ESP_LOGI(LOG_TAG, "ADD_DESCR_EVT, status %d, attr_handle %d, service_handle %d", param->add_char.status, param->add_char.attr_handle, param->add_char.service_handle);
        break;
    
    case ESP_GATTS_START_EVT:
        ESP_LOGI(LOG_TAG, "SERVICE_START_EVT, status %d, service_handle %d",
                 param->start.status, param->start.service_handle);
        break;
    
    case ESP_GATTS_CONNECT_EVT:
        ESP_LOGI(LOG_TAG, "ESP_GATTS_CONNECT_EVT, conn_id %d, remote %02x:%02x:%02x:%02x:%02x:%02x",
                 param->connect.conn_id,
                 param->connect.remote_bda[0], param->connect.remote_bda[1], param->connect.remote_bda[2],
                 param->connect.remote_bda[3], param->connect.remote_bda[4], param->connect.remote_bda[5]);
        lock_service_profile.conn_id = param->connect.conn_id;
        break;
    case ESP_GATTS_DISCONNECT_EVT:
        esp_ble_gap_start_advertising(&lock_adv_params);
        break;
    case ESP_GATTS_DELETE_EVT:
    case ESP_GATTS_EXEC_WRITE_EVT:
    case ESP_GATTS_MTU_EVT:
    case ESP_GATTS_CONF_EVT:
    case ESP_GATTS_UNREG_EVT:
    case ESP_GATTS_ADD_INCL_SRVC_EVT:
    case ESP_GATTS_STOP_EVT:
    case ESP_GATTS_OPEN_EVT:
    case ESP_GATTS_CANCEL_OPEN_EVT:
    case ESP_GATTS_CLOSE_EVT:
    case ESP_GATTS_LISTEN_EVT:
    case ESP_GATTS_CONGEST_EVT:
    default:
        break;
    }
}

//This seems to be some sort of multiplexor that handles callback events and routes them accordingly. TODO understand
static void gatts_event_handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param)
{
    /* If event is register event, store the gatts_if for each profile */
    if (event == ESP_GATTS_REG_EVT) {
        
        if (param->reg.status == ESP_GATT_OK) {
            lock_service_profile.gatts_if = gatts_if;
            ESP_LOGI(LOG_TAG, "Registering interface %d", gatts_if);
        } else {
            ESP_LOGI(LOG_TAG, "Reg app failed, app_id %04x, status %d",
                    param->reg.app_id, 
                    param->reg.status);
            return;
        }
    }

    //ESP_LOGI(LOG_TAG, "Non-Registration Event, interface is %d", gatts_if);

    /* If the gatts_if equal to profile A, call profile A cb handler,
     * so here call each profile's callback */
    do {
        if (gatts_if == ESP_GATT_IF_NONE || gatts_if == lock_service_profile.gatts_if) {
            if (lock_service_profile.gatts_cb) {
                lock_service_profile.gatts_cb(event, gatts_if, param);
            }
        }
    } while (0);
}

void lock_task(void *pvParameter){
    gpio_pad_select_gpio(LOCK_GPIO);
    /* Set the GPIO as a push/pull output */
    gpio_set_direction(LOCK_GPIO, GPIO_MODE_OUTPUT);

    while(1) {
        // Set lock GPIO to match lock state
        if (global_lock_state == LOCK_STATE_MAGIC_VALUE_LOCKED){
            gpio_set_level(LOCK_GPIO, GPIO_HIGH);
        }
        else if (global_lock_state == LOCK_STATE_MAGIC_VALUE_UNLOCKED){
            gpio_set_level(LOCK_GPIO, GPIO_LOW);
            
            // Start autolock countdown if it hasn't started yet
            if (global_autolock_counter == 0){
                global_autolock_counter = LOCK_AUTOLOCK_TIME;
            }
        }
        vTaskDelay(1000 / portTICK_RATE_MS);

        //Trigger autolock countdown
        if (global_lock_state == LOCK_STATE_MAGIC_VALUE_UNLOCKED && global_autolock_counter){
            global_autolock_counter = global_autolock_counter - 1;

            if (global_autolock_counter == 0){
                global_lock_state = LOCK_STATE_MAGIC_VALUE_LOCKED;
                ESP_LOGI(LOG_TAG, "Automatically relocking.");
            }
        }
    }
}

void app_main()
{
    esp_err_t ret;

    // Initialize NVS
    // In theory theres a more robust way to check this, see https://github.com/espressif/esp-idf/blob/master/examples/storage/nvs_rw_value/main/nvs_value_example_main.c for an example. In reality, I'm using like.... 2 KV pairs, so I'm not going to run out anytime soon.
    ret = nvs_flash_init();
    if (ret != ESP_OK){
        ESP_LOGE(LOG_TAG, "%s NVRAM initialization failed", __func__);
        return;
    }


    // Do a check to see if the lock has already been initialized for this level. If so, the stored lock_init value should be equal to the magic value for this level.
    nvs_handle tmp_init_handle;
    uint32_t tmp_init = 0;
    uint32_t tmp_init_len = sizeof(uint32_t);
    ret = nvs_open("storage", NVS_READWRITE, &tmp_init_handle);
    if (ret != ESP_OK) {
        ESP_LOGE(LOG_TAG, "%s failed to read lock_init from NVRAM", __func__);
        return;
    }

    ret = nvs_get_blob(tmp_init_handle, LOCK_INIT_NVS_HANDLE_NAME, &tmp_init, &tmp_init_len);
    ESP_LOGI(LOG_TAG, "Read 0x%08x for lock init with ret 0x%08x", tmp_init, ret);
    nvs_close(tmp_init_handle);

    
    //Load globals from NVRAM as needed, then process the GATT characteristics table (as some permissions will change based on these values, particularly the lock_init state)

    for (int n = 0; n < LOCK_SERVICE_CHARS_NUM; n++){
        if (lock_service_chars[n].is_stored){
            ret = nvs_open("storage", NVS_READWRITE, &lock_service_chars[n].storage_handle);
            if (ret != ESP_OK) {
                ESP_LOGE(LOG_TAG, "%s failed to read value from NVRAM", __func__);
                return;
            }
            if (tmp_init == LOCK_INIT_MAGIC_VALUE){
                if (lock_service_chars[n].char_val.attr_max_len == sizeof(uint32_t)){
                    //uint32_t tmp = 0;
                    ESP_LOGI(LOG_TAG, "Attribute val ptr 0x%08x", (uint32_t)lock_service_chars[n].char_val.attr_value)
                    size_t tmpsize = lock_service_chars[n].char_val.attr_max_len;
                    ret = nvs_get_blob(lock_service_chars[n].storage_handle, lock_service_chars[n].storage_handle_name, (uint32_t *)lock_service_chars[n].char_val.attr_value, &tmpsize);
                    //ret = nvs_get_u32(lock_service_chars[n].storage_handle, lock_service_chars[n].storage_handle_name, &tmp);
                    ESP_LOGI(LOG_TAG, "READ VALUE 0x%08x", *lock_service_chars[n].char_val.attr_value);
                }
            }
        }
    }

    // Handle special cases when the lock is already initialized.
    if (global_lock_init == LOCK_INIT_MAGIC_VALUE){

        //Set init state to read only
        lock_service_chars[LOCK_INIT_CHAR_INDEX].permissions = ESP_GATT_PERM_READ; 
        lock_service_chars[LOCK_INIT_CHAR_INDEX].properties = ESP_GATT_CHAR_PROP_BIT_READ | ESP_GATT_CHAR_PROP_BIT_NOTIFY;

        //Set user pin to read only
        lock_service_chars[USER_PIN_CHAR_INDEX].permissions = ESP_GATT_PERM_READ; 
        lock_service_chars[USER_PIN_CHAR_INDEX].properties = ESP_GATT_CHAR_PROP_BIT_READ | ESP_GATT_CHAR_PROP_BIT_NOTIFY;
    }
    

    


    ESP_LOGI(LOG_TAG, "addr init 0x%08x with value 0x%08x", (uint32_t)&global_lock_init, global_lock_init);


    // Set default (dual mode, single mode not supported yet) config
    esp_bt_controller_config_t bt_cfg = BT_CONTROLLER_INIT_CONFIG_DEFAULT();
    
    // Initialize the HCI
    ret = esp_bt_controller_init(&bt_cfg);
    if (ret) {
        ESP_LOGE(LOG_TAG, "%s initialize controller failed", __func__);
        return;
    }

    // Turn on the HCI
    ret = esp_bt_controller_enable(ESP_BT_MODE_BTDM);
    if (ret) {
        ESP_LOGE(LOG_TAG, "%s enable controller failed", __func__);
        return;
    }

    //Initialize the Bluetooth stack (Bluedroid in this case)
    ret = esp_bluedroid_init();
    if (ret) {
        ESP_LOGE(LOG_TAG, "%s init bluetooth failed", __func__);
        return;
    }

    //Turn on the Bluetooth stack
    ret = esp_bluedroid_enable();
    if (ret) {
        ESP_LOGE(LOG_TAG, "%s enable bluetooth failed", __func__);
        return;
    }

    //Register GATT server callback
    esp_ble_gatts_register_callback(gatts_event_handler);

    //Register GAP callback
    esp_ble_gap_register_callback(gap_event_handler);

    //Register the GATT app
    esp_ble_gatts_app_register(0);

    // Start the lock task
    xTaskCreate(&lock_task, "lock_task", 512, NULL, 5, NULL);

    return;
}
