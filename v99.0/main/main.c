#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_log.h"

#include "nvs_flash.h"
#include "nvs.h"

#define LOG_TAG "FC_LOCK_v99.0"

#define LOCK_NAME_GATT                      "FailCo Lock v1"

#define LOCK_INIT_NVS_HANDLE_NAME           "lock_init"


void app_main()
{
    esp_err_t ret;

    // Initialize NVS
    // In theory theres a more robust way to check this, see https://github.com/espressif/esp-idf/blob/master/examples/storage/nvs_rw_value/main/nvs_value_example_main.c for an example. In reality, I'm using like.... 2 KV pairs, so I'm not going to run out anytime soon.
    ret = nvs_flash_init();
    if (ret != ESP_OK){
        ESP_LOGE(LOG_TAG, "%s NVRAM initialization failed", __func__);
        return;
    }

    nvs_handle tmp_init_handle;
    ret = nvs_open("storage", NVS_READWRITE, &tmp_init_handle);
    if (ret != ESP_OK) {
        ESP_LOGE(LOG_TAG, "%s failed to open NVS", __func__);
        return;
    }

    ret = nvs_erase_key(tmp_init_handle, LOCK_INIT_NVS_HANDLE_NAME);
    ESP_LOGI(LOG_TAG, "ERASING LOCK_INIT STATE, STATUS 0x%08x", ret);
    nvs_commit(tmp_init_handle);
    nvs_close(tmp_init_handle);

    return;
}
